f = @sin;
f2 = @(x) -sin(x);
f2_approx = @(x, h) (f(x+h) - 2*f(x) + f(x-h)) ./ h.^2;

x = pi/6;
h = 10.^-[1:16]';

approx = f2_approx(x, h);
errors = abs(f2(x) - approx);

trunc_err = h.^2;
round_err = eps ./ h.^2;

format short e
disp(table(h, approx, errors, trunc_err, round_err));

explanations = [
    "The error is minimized when the truncation error equals the rounding error."
    "Here, this occurs when h = 1e-4."
    "The the minimal expected error is eps/h^2, or approximately 2e-8."
];

fprintf("\nExplanation:\n");
fprintf("\t* %s\n", explanations);
