f = @sin;
f2 = @(x) -sin(x);

x = pi/6;
h = [0.2 0.1 0.05]';

f2_approx = @(h) (f(x+h) - 2*f(x) + f(x-h) ) ./ (h.^2);
% create a dictionary of computed values so we can reuse them.
f2_approx_vals = dictionary(h, f2_approx(h));
display_results("approximation", h, f2_approx_vals, f2(x));

phi1 = @(h, vals) (4*vals(h/2) - vals(h)) / 3;
h1 = h(1:end-1);
phi1_vals = dictionary(h1, phi1(h1, f2_approx_vals));
display_results("richardson (1 iteration)", h1, phi1_vals, f2(x));
fprintf("\n\tOrder accuracy (h=%g):\t%e\n", 0.2, 0.2^4)

phi2 = @(h, vals) (16*vals(h/2) - vals(h)) / 15;
h2 = h1(1:end-1);
phi2_vals = dictionary(h2, phi2(h2, phi1_vals));
display_results("richardson (2 iterations)", h2, phi2_vals, f2(x));
fprintf("\n\tOrder accuracy (h=%g):\t%e\n", 0.2, 0.2^6)

function display_results(title, h, vals, actual)
    format short e
    fprintf("\n* %s:\n\n", title);
    T = table;
    T.h = h;
    T.vals = vals(h);
    T.abs_error = abs(actual -vals(h));
    T.rel_error = T.abs_error / abs(actual);
    disp(T);
end
