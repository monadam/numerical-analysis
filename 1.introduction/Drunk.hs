module Hw1 (drunk) where

drunk :: Int -> Int -> Int -> Float
-- this function takes:

--    start   integer    the initial position between bar (0) and home (5)
--    end     integer    the target position between bar (0) and home (5)
--    steps:  integer    essentially represents time

-- in exchange, it returns:

--    drunk   float      probability that one would reach given state which drunk

drunk start end 0
  | start == end  = 1
  | otherwise     = 0
drunk start end steps
  | end == 0 =
      (+)
        drunk start 0 (steps - 1)
        drunk start 1 (steps - 1) / 2
  | end == 5 =
      (+)
        drunk start 4 (steps - 1) / 2
        drunk start 5 (steps - 1)
  | otherwise =
      (+)
        drunk start (end - 1) (steps - 1) / 2
        drunk start (end + 1) (steps - 1) / 2

main = do
  print $ drunk 1 1 0
  print $ drunk 2 3 1
  print $ drunk 1 3 2
  print $ drunk 3 0 4
  print $ drunk 3 3 3
