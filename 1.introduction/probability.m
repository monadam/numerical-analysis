bins = zeros(6,1);

for count=1:1000
    k = randi(6);
    bins(k) = bins(k) + 1;
end

A = zeros(100,1);

for n_tries=1:100
    for count=1:1000
        k = randi(6);
        A(n_tries) = A(n_tries) + k; 
    end
    A(n_tries) = A(n_tries)/1000;
end

[mean(A) var(A) std(A)]
