% monte carlo technique birthday problem

N = 10000;

res = zeros(N, 1);

num_ppl = 1;

while mean(res) < 0.5
    for i = 1:N
        bdays = round(randi(365, [num_ppl 1]));
        res(i) = length(unique(bdays)) < num_ppl;
    end

    num_ppl = num_ppl + 1;
end

num_ppl
