theta = linspace(0, 2*pi, 1000);

r = sqrt(2);
x = 2.0 + r*cos(theta);
y = 1.0 + r*sin(theta);
plot(x, y);
axis equal;
hold on;

r = sqrt(3.5);
x = 2.5 + r*cos(theta);
y = 0.0 + r*sin(theta);
plot(x, y);
axis equal;
hold on;

% approx intersect:
% (0.692538, 0.482785)
