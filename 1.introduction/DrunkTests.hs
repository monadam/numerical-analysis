module Drunk (drunk) where

import Drunk
import Test.Hspec

main :: IO ()
main = hspec $ do
  describe "drunk walk" $ do
    it "1" $
      drunk 1 1 0 `shouldBe` (1 :: Float)
    it "2" $
      drunk 2 3 1 `shouldBe` (0.5 :: Float)
    it "3" $
      drunk 1 3 2 `shouldBe` (0.25 :: Float)
    it "4" $
      drunk 3 0 4 `shouldBe` (0.125 :: Float)
    it "5" $
      drunk 3 3 3 `shouldBe` (0.000 :: Float)
