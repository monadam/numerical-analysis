% use monte carlo method to estimate chance of winning craps

N = 10000;

wins = zeros(N, 1);

for i = 1:N
    wins(i) = craps;
end

mean(wins)

bars(wins)

% functions

function res = throw()
    res = randi(6) + randi(6);
end

function [win] = craps
    first_throw = throw;

    if (any([7 11] == first_throw))
        win = true;
    elseif (any([2 3 12] == first_throw))
        win = false;
    else
        win = craps_rec(first_throw);
    end
end

% recursive function

function win = craps_rec(first_throw)
    next_throw = throw;
    if (next_throw == 7)
        win = false;
    elseif (next_throw == first_throw)
        win = true;
    else
        win = craps_rec(first_throw);
    end
end
