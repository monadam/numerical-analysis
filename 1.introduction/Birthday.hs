module Birthday where
import Data.Ratio (Ratio, (%))

birthday :: Integer -> Ratio Integer
birthday k  = 1 - (uniques % total)
  where
    n       = 365
    uniques = product [n - k + 1 .. n]
    total   = n ^ k

solve :: Integer -> Integer
solve k
  | birthday k >= 0.5 = k
  | otherwise         = solve $ k + 1

main :: IO ()
main = do
  putStrLn $ (show . solve) 1 ++ " people"
