% the function we want to find the zero of.
% this one solves for the sqrt(2)
f = @(x) x^2 - 2;

% an interval that contains the zero.
a = 1;
b = 2;

for i = 1 : 100
    % find the center
    c = (b + a) / 2;

    % do f(c) and f(a) have the same sign?
    if (f(c) * f(a) > 0)
        a = c;
    else
        b = c;
    end
end
