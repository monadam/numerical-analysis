f = @(x) cos(x.^2);

a = 0;
b = 1;

process_rule(@trapezoid, f, a, b);
process_rule(@simpson, f, a, b);

function res = trapezoid(f, a, b, n)
    h = (b-a)/n;
    xs_inner = linspace(a+h, b-h, n-1);
    res = h/2 * (f(a) + 2*sum(f(xs_inner)) + f(b));
end

function res = simpson(f, a, b, n)
    assert(mod(n, 2) == 0, 'n must be even')
    h = (b-a)/n;
    xs_odd = linspace(a+h, b-h, n/2);
    xs_even = linspace(a+2*h, b-2*h, n/2-1);
    res = h/3 * (f(a) + 4*sum(f(xs_odd)) + 2*sum(f(xs_even)) + f(b));
end

function process_rule(rule, f, a, b)
    iterations = 8;

    n = 2.^[1:iterations]';
    h = (b-a)./n;

    actual = quad(f, a, b, [1.e-12, 1.e-12]);
    result = zeros(iterations, 1);
    rule_name = func2str(rule);

    for i = 1:iterations
        result(i) = rule(f, a, b, n(i));
    end

    error = abs(actual - result);

    if strfind(rule_name, 'trapezoid')
        ratio = error ./ h.^2;
    elseif strfind(rule_name, 'simpson')
        ratio = error ./ h.^4;
    else
        error("invalid rule: %s", rule_name);
    end

    format;
    fprintf('* %s\n\n', rule_name);
    disp(table(h, result, error, ratio));
end
