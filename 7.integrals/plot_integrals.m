clear all;
a = -1;
b = 1;
domain = linspace(a, b, 10^5);
ns = 2.^[0:3]';

f = @(x) 1./(1+x.^2) + sin(x*10)/5

close all;
layout = tiledlayout(length(ns), 2);
layout.TileSpacing = 'compact';
layout.Padding = 'compact';

title(layout, "HOMEWORK 11", "Adam Neeley")

for i = 1:length(ns)
    nexttile(i*2-1);
    hold on
    drawTrapezoids(f, a, b, ns(i));
    plotF(f, domain);
    nexttile(i*2);
    hold on
    drawSimpsons(f, a, b, ns(i));
    plotF(f, domain);
end

function drawTrapezoids(f, a, b, n)
    colors = [0 1 0; 0 .8 0]
    xs = linspace(a, b, n+1);
    for i = 2:length(xs)
        x = xs(i);
        x_prev = xs(i-1);
        color = colors(mod(i, 2)+1, :);
        patch([x_prev x_prev x x], [0 f(x_prev) f(x) 0], color, 'LineStyle', 'none', 'FaceAlpha', 0.8);
    end
end
function drawSimpsons(f, a, b, n)
    colors = [.6 .8 1; .5 .7 1]
    xs = linspace(a, b, n+1);
    for i = 2:length(xs)
        x = xs(i)
        x_prev = xs(i-1);
        x_mid = (x + x_prev) / 2;
        color = colors(mod(i, 2)+1, :);
        patch([x_prev x_prev x_mid x x], [0 f(x_prev) f(x_mid) f(x) 0], color, 'LineStyle', 'none', 'FaceAlpha', 0.8);
    end
end

function plotF(f, domain)
    plot(domain, f(domain), 'r', 'LineWidth', 2);
end
