function [ res, est ] = my_romberg(f, a, b, tol)

% initial values
n = 1;
h = b - a;
fs = f([a; b]);
est(1, 1) = h/2 * sum(fs);

err = Inf;

for iter = 1 : 10
    if err <= tol; break; end

    n = 2 * n;
    h = h / 2;

    % load previous values of f
    fs_new = zeros(n+1, 1);
    fs_new(1:2:n+1) = fs(1:n/2+1);

    % add values in-between
    for j = 2:2:n
        fs_new(j) = f(a+(j-1)*h);
    end

    % combine values
    fs = fs_new;

    % calc endpoints
    trap = 0.5*(fs(1) + fs(n+1));

    % calc inner
    trap = trap + sum(fs(2:n));

    % romberg algorithm
    est(iter+1, 1) = h*trap;
    for j = 1 : iter
        est(iter+1, j+1) = ((4^(j))*est(iter+1,j) - est(iter,j))/(4^j-1);
    end

    % update result
    res = est(iter+1,iter+1);
    err = max([
                abs(res - est(iter,   iter))
                abs(res - est(iter+1, iter))
              ]);
end
end
