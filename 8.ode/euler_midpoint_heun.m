a = 0;
b = 15;
y0 = [2; 2];

h = 1e-1;

tiledlayout(3, 2);

ts = linspace(a, b, (b-a)/h);

ys = zeros(2, length(ts));
ys(:, 1) = y0;

ys_euler = euler(ys, h);
ys_midpoint = midpoint(ys, h);
ys_heun = heun(ys, h);

nexttile
hold on
plot(ts, ys_euler(1, :))
plot(ts, ys_euler(2, :))
nexttile
plot(ys_euler(1, :), ys_euler(2, :))

nexttile
hold on
plot(ts, ys_midpoint(1, :))
plot(ts, ys_midpoint(2, :))
nexttile
plot(ys_midpoint(1, :), ys_midpoint(2, :))

nexttile
hold on
plot(ts, ys_heun(1, :))
plot(ts, ys_heun(2, :))
nexttile
plot(ys_heun(1, :), ys_heun(2, :))

function y = f(y)
    assert(size(y, 1) == 2, 'y should be a 2-row')
    R = y(1);
    F = y(2);
    y = [
     R - 0.01 * R * F;
     0.2 * R * F - F
    ];
end

function ys = euler(ys, h)
    for k = 2 : size(ys, 2);
        y = ys(:, k-1);
        ys(:, k) = y + h * f(y);
    end
end

function ys = midpoint(ys, h)
    for k = 2 : size(ys, 2);
        y = ys(:, k-1);
        ys(:, k) = y + h * f(y + h/2 * f(y));
    end
end

function ys = heun(ys, h)
    for k = 2 : size(ys, 2);
        y = ys(:, k-1);
        f_y = f(y);
        ys(:, k) = y + h/2 * (f_y + f(y + h * f_y));
    end
end
