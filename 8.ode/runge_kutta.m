close all;
clear all;

L = tiledlayout(3, 1);
L.Padding = 'compact';
L.TileSpacing = 'compact';

nexttile;
hold on;
plotZ( euler (21,   0.0025, 4,  0.5i));
plotZ( euler (14,   0.0025, 3,  0.5i));
plotZ( euler (7,    0.0025, 2,  0.5i));

nexttile;
hold on;
plotZ( rungk (50,   0.25,   4,  0.5i));
plotZ( rungk (150,  0.5,    4,  0.6i));
plotZ( rungk (200,  0.5,    4,  0.8i));
plotZ( rungk (35,   0.25,   4,  0.4i));
plotZ( rungk (30,   0.25,   4,  0.2i));
plotZ( rungk (30,   0.05,   4,  0.2i));

nexttile;
hold on;
plotZ( myode (50,   0.25,   4,  0.5i));
plotZ( myode (150,  0.5,    4,  0.6i));
plotZ( myode (200,  0.5,    4,  0.8i));
plotZ( myode (35,   0.25,   4,  0.4i));
plotZ( myode (30,   0.25,   4,  0.2i));
plotZ( myode (30,   0.05,   4,  0.2i));

function zs = euler(t_max, h, z0, w0)
    f_w = @(z) -z / abs(z)^3;
    f_z = @(w) w;

    n  = t_max / h;
    zs = zeros(n, 1);
    ws = zeros(n, 1);
    zs(1) = z0;
    ws(1) = w0;

    for k = 1 : n-1
        z_k = zs(k);
        w_k = ws(k);
        zs(k+1) = z_k + h * f_z(w_k);
        ws(k+1) = w_k + h * f_w(z_k);
    end
end

function zs = rungk(t_max, h, z0, w0)
    f_w = @(z) -z / abs(z)^3;
    f_z = @(w) w;

    n = t_max / h;
    zs = zeros(n, 1);
    ws = zeros(n, 1);
    zs(1) = z0;
    ws(1) = w0;

    for k = 1 : n-1
        w_k = ws(k);
        z_k = zs(k);

        w_q1 = f_w(z_k);
        w_q2 = f_w(z_k + h/2 * w_q1);
        w_q3 = f_w(z_k + h/2 * w_q2);
        w_q4 = f_w(z_k + h   * w_q3);
        ws(k+1) = w_k + h/6 * (w_q1 + 2*w_q2 + 2*w_q3 + w_q4);

        z_q1 = f_z(w_k);
        z_q2 = f_z(w_k + h/2 * z_q1);
        z_q3 = f_z(w_k + h/2 * z_q2);
        z_q4 = f_z(w_k + h   * z_q3);
        zs(k+1) = z_k + h/6 * (z_q1 + 2*z_q2 + 2*z_q3 + z_q4);
    end
end

function zs = myode(t_max, h, z0, w0)
    f_w = @(z) -z / abs(z)^3;
    [t, v] = ode45(@(t, v) [v(2); f_w(v(1))], [0, t_max], [z0; w0]);
    zs = v(:, 1);
end

function p = plotZ(zs)
    p = plot(real(zs), imag(zs));
    axis equal;
    grid on;
    xlim([-5, 5]);
    ylim([-5, 5]);
end
