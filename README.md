This repository contains some code from a numerical analysis class.

1. introductory material
2. approximating numbers
3. condition numbers
4. linear algebra
5. polynomial interpolation
6. derivatives
7. integrals
8. differential equations
