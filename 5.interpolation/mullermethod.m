domain = linspace(-1, 3, 100)

f = @(x) x.^3 - 2
xs = [0 1 2];
ys = f(xs);

fImage = f(domain);
diffs = divDiffs([xs; ys]')

% plot stuff
color.data = 'b'
color.secant = [0 .8 0]
color.muller = 'r'

clf
hold on;
grid on
xline(0)
yline(0)
xlim([-.2 2.2])
ylim([-3 8])

% plot data
plot(xs, ys, 'ko')
% draw labels
for m = [1 2 3]
    text(xs(m), ys(m), pStr(m-1))
end
plot(domain, fImage, color.data)

ANSWER = divDiffs([0 1 2; -2 -1 6]')

% plot secant
% plotSecant(xs, f, domain)
plotMuller(xs, f, domain)

function plotMuller(xs, f, domain)
assert(length(xs) >= 3)
xs = xs(end-2:end)
x0 = xs(1)
x1 = xs(2)
x2 = xs(3)
diffs = divDiffs([xs; f(xs)]')
a0 = diffs(1)
a1 = diffs(2)
a2 = diffs(3)
% x4 = x3 - f(x2) / diffs(2)
% sec = @(x) (x-x3) * diffs(2)
% plot(x3, 0, '.', 'Color', 'g', 'MarkerSize', 2)
mullerplot = plot(xs, f(xs));
% mullerplot = plot(domain, interp1(xs, f(xs), domain, 'pchip'));
color = [1 0.5 0];
mullerplot.Color = color;
mullerplot.LineWidth = 3;
mullerplot.LineStyle = ':';
end

function plotSecant(xs, f, domain)
x1 = xs(end-1)
x2 = xs(end)
diffs = divDiffs([x1 x2; f([x1 x2])]')
x3 = x2 - f(x2) / diffs(2)
sec = @(x) (x-x3) * diffs(2)
plot(x3, 0, '.', 'Color', 'g', 'MarkerSize', 2)
secantplot = plot(domain, sec(domain));
color = [0 0.5 0]
secantplot.Color = color
secantplot.LineStyle = '-'
end

function str = pStr(n)
xStr = sprintf('x_%d', n);
str = sprintf('\n\n (%s, f(%s))', xStr, xStr);
end

function as = divDiffs(ps)
k = length(ps);
dds = zeros(k);
dds(:, 1) = ps(:, 2); % set first column to ys
for j = 2 : k % loop from second column
    for i = 1 : k-j+1 % (triangular matrix)
        dd2 = dds(i+1, j-1);
        dd1 = dds(i, j-1);
        x2 = ps(i+j-1, 1);
        x1 = ps(i, 1);
        dds(i, j) = (dd2 - dd1) / (x2 - x1 );
    end
end
as = dds(1, :); % top row has our values
end

