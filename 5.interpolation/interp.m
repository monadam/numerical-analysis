% data
x = [0 20 40 60 80 100]';
y = [76.0 105.7 131.7 179.3 226.5 281.4]';
year2020 = 120;
pop2020 = 330;

% generate constants for polynomial
c = vander(x) \ y;

% create polynomial from constants
% and apply to new interval
x2 = (0:year2020)';
p = polyval(c, x2);

% calculate prediction
predict2020 = p(year2020)

% plot
plot(x, y, 'o', ...
    x2, p, 'k', ...
    year2020, pop2020, 'ro');
