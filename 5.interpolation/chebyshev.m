f = @(x) 1 ./ (1 + x.^2);

xn = @(n) linspace(-5, 5, n);
xj = @(len) 5 * cos([0:len] * pi / len);

domain = xn(99);

x12 = xn(13);
xj12 = xj(12);
xj20 = xj(20);

% SETUP
clf;
layout = tiledlayout(4, 2);
layout.Padding = 'compact';
layout.TileSpacing = 'compact';

% plot runge function
nexttile([1 2]);
plot(domain, f(domain));
title("Runge function");

% PART A
pa = polyfit(x12, f(x12), 12);
plotPoly(pa, "P_{12} Linear");

% PART B
pb = polyfit(xj12, f(xj12), 12);
plotPoly(pb, "P_{12} Chebyshev");

% PART C
pc = polyfit(xj20, f(xj20), 20);
plotPoly(pc, "P_{20} Chebyshev");

function plotPoly(p, name)
    domain = evalin("caller", "domain");
    actual = evalin("caller", "f(domain)");

    vals = polyval(p, domain);

    absErr = abs((actual - vals));
    relErr = absErr ./ abs(actual);
    
    nexttile;
    plot(domain, vals);
    title(name);
    xlim([-5 5]);
    ylim([0 1])

    nexttile;
    hold on;
    plot(domain, relErr, 'Color', [1 0.5 0]);
    plot(domain, absErr, 'Color', [1 0 0]);
    title("error");
end
