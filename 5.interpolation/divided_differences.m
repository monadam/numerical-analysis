function as = div_diffs(ps)
    k = length(ps);
    dds = zeros(k);
    dds(:, 1) = ps(:, 2); % set first column to ys
    for j = 2 : k % loop from second column
        for i = 1 : k-j+1 % (triangular matrix)
            dd2 = dds(i+1, j-1);
            dd1 = dds(i, j-1);
            x2 = ps(i+j-1, 1);
            x1 = ps(i, 1);
            dds(i, j) = (dd2 - dd1) / (x2 - x1 );
        end
    end
    as = dds(1, :); % top row has our values
end
