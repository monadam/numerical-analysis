clf;
tl = tiledlayout(2, 2);
tl.Padding = 'compact';
tl.TileSpacing = 'compact';

% plot A
nexttile
pathA = [
    1+1i
    2
    3+1i
    2+2.5i
    1.2+3.4i
    2+4i
    2.7+3.2i
];

for j=1:5
    for k=0:3
        path = pathA+2*k*1i;
        path = pathflip(path*k, j * pi / 5);
        plotpath(path);
    end
end

% plot B
nexttile;
pathB = [
    3+4i
    1.75+1.6i
    0.9+0.5i
    0.1+0i
    0.5+1i
    1.5+.5i
    3.25+.5i
    4.25+2.25i
    4.25+4i
    3+4i
    3.75+3.25i
    6+4.25i
];

w = real(pathsize(pathB));
plotpath([pathB; pathB + w; pathB + 2*w]);

% plot C
nexttile;
k = [0:2*pi/3:2*pi];
pathC = -1+exp(1i*k);
plotpath(pathC);
plotpath(pathflip(pathC, pi/3));
plotpath(pathflip(pathC, -pi/3));

% plot D
nexttile;
k = 1:10;
plotpath(exp(k.*1i*pi/2)  ./ k);

% functions
function p = pathsize(p)
xs = real(p);
ys = imag(p);
w = max(xs) - min(xs);
h = max(ys) - min(ys);
p = w + 1i*h;
end

function p = pathflip(p, theta)
p = pathrotate(p, theta);
p = conj(p);
p = pathrotate(p, -1 * theta);
end

function p = pathrotate(p, theta)
p = p * exp(1i*theta);
end

function plotpath(p)
n = length(p);
t = 0:n-1;
tt = 0:0.1:n-1;

sr = spline(t, real(p), tt)';
si = spline(t, imag(p), tt)';

hold on;
axis equal;

plot(sr, si);
plot(real(p), imag(p), '.');
% comet(sr, si);
end
