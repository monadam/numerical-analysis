f = @(x) 1 ./ (1 + x.^2);
df = @(x) -2 * x * f(x) .^ 2;
xn = @(n) linspace(-5, 5, n);
domain = xn(100);
x13 = xn(13);

layout = tiledlayout(4, 2);
layout.TileSpacing = 'compact';
layout.Padding = 'compact';

linearVals =  interp1(x13, f(x13), domain, 'linear');
hermiteVals = interp1(x13, f(x13), domain, 'pchip');
myHermiteVals = myHermite(x13, f, df, domain);
splineVals =  interp1(x13, f(x13), domain, 'spline');

plotVals(linearVals, 'linear');
plotVals(hermiteVals, 'hermite');

plotVals(myHermiteVals, "adam's hermite")
plotVals(splineVals, 'spline');

function [left, right] = findSegment(xs, x)
assert(x >= min(xs));
assert(x <= max(xs));
for i = 2 : length(xs)
    left = xs(i-1);
    right = xs(i);
    if (left <= x && x <= right)
        break;
    end
end
end

function vals = myHermite(xs, f, df, domain)
h = xs(2) - xs(1);
vals = zeros(size(domain));
for i = 1 : length(domain)
    x = domain(i);
    [x1, x2] = findSegment(xs, x);
    a = 3/h^2 * (df(x1) + df(x2)) + 6 / h^3 * (f(x1) - f(x2));
    vals(i) = sum([
        -1 * df(x1) / (2*h) * ((x - x2)^2 - h^2)
        df(x2) * (x - x1)^2 / (2 * h)
        a * (x-x1)^2 * ((x-x1)/3 - h/2)
        f(x1)
    ]);
end
end

function plotVals(vals, name)
domain = evalin("caller", "domain");
actual = evalin("caller", "f(domain)");

absErr = abs((actual - vals));
relErr = absErr ./ abs(actual);

nexttile;
plot(domain, vals);
title(name);
xlim([-5 5]);
ylim([0 1])

nexttile;
hold on;
plot(domain, relErr, 'Color', [1 0.5 0]);
plot(domain, absErr, 'Color', [1 0 0]);
title("error");
end
