xs = linspace(0, pi, 1000)';
ys = sin(xs);
clf;
rs = pi * rand(100, 1);
estimate = zeros(size(rs));

for j = 1 : length(rs)
    r = rs(j);
    % find interval
    for i = 2 : length(xs)
        left = xs(i-1);
        right = xs(i);
        if (left <= r && r <= right)
            break;
        end
    end
    estimate(j) = (ys(i-1) + ys(i)) / 2; % FIXME
end

actual = sin(rs);
absError = abs(actual - estimate);
relError = absError ./ abs(actual);

maxAbsError = max(absError)
maxRelError = max(relError)

nexttile;
hold on;
domain = 1:length(rs);
plot(domain, estimate, 'b');
hold on;
plot(domain, actual, 'r.');
