% newton.m
% polynomial interpolation
% define some constants
F1 = @(x) x .* sin(x * pi / 2);
F2 = @(x) 1 ./ (1 + x.^2);
F3 = @(x) exp(x);
F4 = @(x) sin(x .* pi / 2) ./ (x*pi/2+eps);
F = F2;

STEPS = 100;

% data
xs = -5:5;
ys = F(xs);
% ys = randn(size(xs))
pts = [xs' ys'];

% interpolation
tic; % about 0.02-0.03 seconds
ixs = linspace(min(xs)+1, max(xs)-1, STEPS);
iys = interp(pts, ixs);
ipts = [ixs' iys'];
toc;

% basic tests
testData = [1 2; 2 3; 3 6];
assert(isequal([2 1 1], divDiffs(testData)));
assert(isequal([2 3 6], interp(testData, [1 2 3])));

% plot
plotStuff(pts, ipts, F);

% functions
function res = interp(ps, x)
k = length(ps);
terms = zeros(k, length(x));
as = divDiffs(ps);
terms(1, :) = as(1);
for i = 2:k
    for xi = 1:length(x)
        terms(i, xi) = as(i) * prod(x(xi) - ps(1:i-1, 1));
    end
end
res = sum(terms);
end

function as = divDiffs(ps)
k = length(ps);
dds = zeros(k);
dds(:, 1) = ps(:, 2); % set first column to ys
for j = 2 : k % loop from second column
    for i = 1 : k-j+1 % (triangular matrix)
        dd2 = dds(i+1, j-1);
        dd1 = dds(i, j-1);
        x2 = ps(i+j-1, 1);
        x1 = ps(i, 1);
        dds(i, j) = (dd2 - dd1) / (x2 - x1 );
    end
end
as = dds(1, :); % top row has our values
end

function plotStuff(pts, ipts, f) 
close all
hold on;
xs = pts(:, 1);
ys = pts(:, 2);
ixs = ipts(:, 1);
iys = ipts(:, 2);
plot(ixs, f(ixs), 'g', 'DisplayName', func2str(f)); % actual
plot(xs, ys, 'k:', 'DisplayName', 'linear'); % linear
plot(ixs, iys, 'b', 'DisplayName', 'polynom'); % poly
plot(xs, ys, 'r.', 'DisplayName', 'data'); % points
legend
end