module Cosines where

nCosines :: (Floating a) => Int -> [a]
nCosines n = take n $ iterate cos pi

main :: IO ()
main = printLines $ nCosines 200
  where
    printLines = putStrLn . concatMap ((++) "\n" . show)
