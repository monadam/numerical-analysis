module Hw3 where

import Text.Printf ( printf )

question2 :: IO ()
question2 = let
    f :: Double -> Double
    f x = (5 - x) * exp x - 5
  in do
    let
      a = 4
      b = 5
      delta1 = 5e-7
      delta2 = 5e-13
      steps :: Double -> Int
      steps = floor . logBase 0.5
      printSteps delta = printf "\ndelta\t%-7e\nsteps\t%2d\n" delta (steps delta)
      in do
        printf "\nPart A\n"
        bisect f a b delta1
        bisect f a b delta2
        printSteps delta1
        printSteps delta2
        printSteps 1e-16

    let
      f' :: Double -> Double
      f' x = (4 - x) * exp x
      in do
        printf "\nPart B\n"
        newton f f' 10e-8 4.9651142317442763037 5
        printf "\nSince the decay is quadratic, decreasing delta from 10e-8 to its square 10e-16 should lead to one additional step.\n"

bisect :: (Double -> Double) -> Double -> Double -> Double -> IO ()
bisect f a0 b0 delta = do
  printf "\nBisect method\n"
  printf "delta: %e\n" delta
  bis a0 b0 0
  where
    bis :: Double -> Double -> Int -> IO ()
    bis a b step
      | fa * fb > 0 = print "error"
      | d >= delta = do
        printState
        if signum fc == signum fa
          then bis c b (step + 1)
          else bis a c (step + 1)
      | otherwise = printState
        where
          fa = f a
          fb = f b
          fc = f c
          c = (a + b) / 2
          d = (b - a) / 2
          printState = printf "%-3d %.12g  %.12g\n" step a b


newton :: (Double -> Double) -> (Double -> Double) -> Double -> Double -> Double -> IO ()

newton f f' delta actual guess = do
  printf "\nNewton's method:\n\n"
  newt 0 guess
  where
    newt :: Integer -> Double -> IO ()
    newt step x
      | abs err < delta = do
          printStep
      | otherwise = do
          printStep
          newt (step + 1) (x - (f x / f' x))
        where
          printStep = do
            printf "%-3d\t%g\n" step x
            printf "error:  %-.1g\n" err
          err = abs $ actual - x

question4 :: IO()
question4 = do
  newton (\x -> x * x - 2) (* 2) 1e-7 (sqrt 2) 2
