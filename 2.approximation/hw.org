#+title: homework 3
* Question 2
#+begin_src matlab
f = @(x) (5-x) .* exp(x) - 5;

d = 0:0.1:5;

res = f(d);

plot(res)
#+end_src
[[./q2.png]]
#+begin_src haskell
module Hw3 where

import Text.Printf ( printf )

question2 :: IO ()
question2 = let
    f :: Double -> Double
    f x = (5 - x) * exp x - 5
  in do
    let
      a = 4
      b = 5
      delta1 = 5e-7
      delta2 = 5e-13
      steps :: Double -> Int
      steps = floor . logBase 0.5
      printSteps delta = printf "\ndelta\t%-7e\nsteps\t%2d\n" delta (steps delta)
      in do
        printf "\nPart A\n"
        bisect f a b delta1
        bisect f a b delta2
        printSteps delta1
        printSteps delta2
        printSteps 1e-16

    let
      f' :: Double -> Double
      f' x = (4 - x) * exp x
      in do
        printf "\nPart B\n"
        newton f f' 10e-8 4.9651142317442763037 5
        printf "\nSince the decay is quadratic, decreasing delta from 10e-8 to its square 10e-16 should lead to one additional step.\n"

bisect :: (Double -> Double) -> Double -> Double -> Double -> IO ()
bisect f a0 b0 delta = do
  printf "\nBisect method\n"
  printf "delta: %e\n" delta
  bis a0 b0 0
  where
    bis :: Double -> Double -> Int -> IO ()
    bis a b step
      | fa * fb > 0 = print "error"
      | d >= delta = do
        printState
        if signum fc == signum fa
          then bis c b (step + 1)
          else bis a c (step + 1)
      | otherwise = printState
        where
          fa = f a
          fb = f b
          fc = f c
          c = (a + b) / 2
          d = (b - a) / 2
          printState = printf "%-3d %.12g  %.12g\n" step a b

newton :: (Double -> Double) -> (Double -> Double) -> Double -> Double -> Double -> IO ()

newton f f' delta actual guess = do
  printf "\nNewton's method:\n\n"
  newt 0 guess
  where
    newt :: Integer -> Double -> IO ()
    newt step x
      | abs err < delta = do
          printStep
      | otherwise = do
          printStep
          newt (step + 1) (x - (f x / f' x))
        where
          printStep = do
            printf "%-3d\t%g\n" step x
            printf "error:  %-.1g\n" err
          err = abs $ actual - x
#+end_src
** Result
#+begin_src
Part A

Bisect method
delta: 5.0e-7
0   4.000000000000  5.000000000000
1   4.500000000000  5.000000000000
2   4.750000000000  5.000000000000
3   4.875000000000  5.000000000000
4   4.937500000000  5.000000000000
5   4.937500000000  4.968750000000
6   4.953125000000  4.968750000000
7   4.960937500000  4.968750000000
8   4.964843750000  4.968750000000
9   4.964843750000  4.966796875000
10  4.964843750000  4.965820312500
11  4.964843750000  4.965332031250
12  4.965087890625  4.965332031250
13  4.965087890625  4.965209960938
14  4.965087890625  4.965148925781
15  4.965087890625  4.965118408203
16  4.965103149414  4.965118408203
17  4.965110778809  4.965118408203
18  4.965110778809  4.965114593506
19  4.965112686157  4.965114593506
20  4.965113639832  4.965114593506

Bisect method
delta: 5.0e-13
0   4.000000000000  5.000000000000
1   4.500000000000  5.000000000000
2   4.750000000000  5.000000000000
3   4.875000000000  5.000000000000
4   4.937500000000  5.000000000000
5   4.937500000000  4.968750000000
6   4.953125000000  4.968750000000
7   4.960937500000  4.968750000000
8   4.964843750000  4.968750000000
9   4.964843750000  4.966796875000
10  4.964843750000  4.965820312500
11  4.964843750000  4.965332031250
12  4.965087890625  4.965332031250
13  4.965087890625  4.965209960938
14  4.965087890625  4.965148925781
15  4.965087890625  4.965118408203
16  4.965103149414  4.965118408203
17  4.965110778809  4.965118408203
18  4.965110778809  4.965114593506
19  4.965112686157  4.965114593506
20  4.965113639832  4.965114593506
21  4.965114116669  4.965114593506
22  4.965114116669  4.965114355087
23  4.965114116669  4.965114235878
24  4.965114176273  4.965114235878
25  4.965114206076  4.965114235878
26  4.965114220977  4.965114235878
27  4.965114228427  4.965114235878
28  4.965114228427  4.965114232153
29  4.965114230290  4.965114232153
30  4.965114231221  4.965114232153
31  4.965114231687  4.965114232153
32  4.965114231687  4.965114231920
33  4.965114231687  4.965114231803
34  4.965114231687  4.965114231745
35  4.965114231716  4.965114231745
36  4.965114231731  4.965114231745
37  4.965114231738  4.965114231745
38  4.965114231742  4.965114231745
39  4.965114231743  4.965114231745
40  4.965114231743  4.965114231744

delta   5.0e-7
steps   20

delta   5.0e-13
steps   40

delta   1.0e-16
steps   53

Part B

Newton's method:

0       5.0
error:  3.5e-2
1       4.966310265004573
error:  1.2e-3
2       4.965115686301458
error:  1.5e-6
3       4.96511423174643
error:  2.2e-12

Since the decay is quadratic, decreasing delta from 10e-8 to its square 10e-16 should lead to one additional step.
#+end_src
* Question 4
#+begin_src haskell
question4 :: IO()
question4 = do
  newton (\x -> x * x - 2) (* 2) 1e-7 (sqrt 2) 2
#+end_src
#+begin_src
ghci> question4

Newton's method:

0       2.0
error:  0.6
1       1.5
error:  8.6e-2
2       1.4166666666666667
error:  2.5e-3
3       1.4142156862745099
error:  2.1e-6
4       1.4142135623746899
error:  1.6e-12
#+end_src
* Questions 6, 7, 9, 10, 18
[[./resize_paper1.jpg]]
[[./resize_paper2.jpg]]
** Question 10
*** Part 1
#+begin_src
ghci> bisect (\x -> (x ^ 2 - 2 * x + 1) / (x ^ 2 - x - 2)) 0 3 1e-3

Bisect method
delta: 1.0e-3
0   0.000000000000  3.000000000000
1   1.500000000000  3.000000000000
2   1.500000000000  2.250000000000
3   1.875000000000  2.250000000000
4   1.875000000000  2.062500000000
5   1.968750000000  2.062500000000
6   1.968750000000  2.015625000000
7   1.992187500000  2.015625000000
8   1.992187500000  2.003906250000
9   1.998046875000  2.003906250000
10  1.998046875000  2.000976562500
11  1.999511718750  2.000976562500
#+end_src
*** Part 2
[[./q10.png]]
** Question 18
#+begin_src matlab
x0 = 1;

P0 = @(x) (1 + x - x); % hack
P1 = @(x) (P0(x) - 2 .* (x - x0));
P2 = @(x) (P1(x) + (x - x0) .^ 2);
P3 = @(x) (P2(x) + (4/3) * (x - x0) .^ 3);

f = @(x) (exp(1 - x .* x));
d = 0:0.1:2;


subplot(2, 2, 1);
plot(f(d))
hold on;
plot(P0(d));

subplot(2, 2, 2);
plot(f(d))
hold on;
plot(P1(d));

subplot(2, 2, 3);
plot(f(d))
hold on;
plot(P2(d));

subplot(2, 2, 4);
plot(f(d))
hold on;
plot(P3(d));
#+end_src
[[./q18.png]]
* Question 19
[[./resize_paper3.jpg]]
#+begin_src matlab
fprintf("\n\nEstimating pi:\n");

for n = 1 : 2: 11
    Tn = 16 * Pn(n, 1/5) - 4 * Pn(n, 1/239);
    error = abs(Tn - pi);

    fprintf("\nT%-2d   %.9f\n", n, Tn);
    fprintf("error %.9f\n", error);
end

function [result] = Pn(n, x)
res = 0;
for k = 0 : floor((n - 1) / 2)
j = 2 * k + 1;
res = res + ((-1) .^ k) .* (x .^ j) ./ j;
end
result = res;
end
#+end_src

** Results
#+begin_src
Estimating pi:

T1    3.183263598
error 0.041670945

T3    3.140597029
error 0.000995624

T5    3.141621029
error 0.000028376

T7    3.141591772
error 0.000000881

T9    3.141592682
error 0.000000029

T11   3.141592653
error 0.000000001
#+end_src

