module Hw4 where

type Val = Double
type Fn = Val -> Val

secant :: Fn -> (Val, Val) -> (Val, Val)
-- evaluate one iteration of the secant method
secant f (x0, x1) = (x1, x2)
  where x2 = x1 - f x1 * (x1 - x0) / (f x1  - f x0 )

question2c :: IO ()
question2c = mapM_ print
             $ take 6
             $ map (\t -> (snd t, err ( snd t )))
             $ iterate (secant f) (4, 5)
  where
    f x = (5-x) * exp x - 5
    actual = 4.9651142317442763037
    delta = 1e-8
    err x = abs (actual - x)
    notPrecise t = err ( fst t ) > delta

question5 :: IO ()
question5 =  print $ take 10 $ iterate (secant f) (0, 1)
  where
    f x = x^2 - 3
    actual = 4.9651142317442763037
    delta = 1e-8
    err x = abs (actual - x)
    notPrecise t = err ( fst t ) > delta


x_ = 4.9651142317442763037
f x = (5-x) * exp x - 5
f' x = f x - exp x
f'' x = f' x - exp x
c_ = abs $ ( f'' x_ ) / ( 2 * f' x_ )

main = print c_


question14 = mapM_ print $ take 100 $ iterate cos pi
