A0 = [1e-16 1; 1 1];
b0 = [2; 3];


nopivot(A0, b0)
A0 \ b0
A0 = [1 2; 5 7];
b0 = [1; 2];

A1 = [10^-16 1; 1 1];
b1 = [2; 3];

disp("ge pivot")

A1\b1

disp("nopivot")

[A, b] = genopivot(A1, b1)

usolve(A, b)

function [A, b] = genopivot(A_, b_)
A = A_;
b = b_;
n = length(A);

for j=1:n-1
    for i=j+1:n
        mult = A(i,j) / A(j, j);
        A(i,:) = A(i,:) - mult*A(j,:);
        b(i,:) = b(i) - mult*b(j);
    end
end
end


function y = usolve(U, b)
n = length(b);
% start at last row
for row = n:-1:1
    % U isn't unit diagonal
    y(row) = b(row) / U(row, row);
    % start at last column
    for col = n:-1:row+1
        y(row) = y(row) - U(row, col) * y(col);
    end
end
end

function x = nopivot(A, b)
n = length(A);
for i=1:n-1
    mult = A(i+1:n, i) / A(i,i);
    A(i+1:n,:) = A(i+1:n,:) - mult * A(i,:);
    b(i+1:n,:) = b(i+1:n,:) - mult * b(i,:);
end
% x = zeros(n, 1);
% x(n,:) = b(n,:) / A(n,n);
% for i = n-1 : -1 : 1
%     x(i, :) = (b(n,:) - A(i, i+1:n) * x(i+1:n,:)) / A(i, i);
% end
x = usolve(A, b);
end
