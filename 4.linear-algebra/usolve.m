% some tests

U2 = [1 1; 0 3];
b2 = [2; 3];
y2 = [1; 1];

U3 = [1 2 1; 0 1 1; 0 0 2];
b3 = [4; 2; 2];
y3 = [1; 1; 1];

U4 = [1 2 3 4; 0 2 0 0; 0 0 1 1; 0 0 0 2];
b4 = [10; 2; 2; 2];
y4 = [1; 1; 1; 1];

usolve(U2, b2)
usolve(U3, b3)
usolve(U4, b4)

function x = usolve(U, b)
ops = 0;
n = length(b);
x = b;
for row = n:-1:1
    x(row) = b(row) / U(row, row);
    ops = ops + 1;
    for col = n:-1:row+1
        x(row,1) = x(row,1) - U(row, col) * x(col,1);
        ops = ops + 2;
    end
end
fprintf("operations: %d\n", ops)
end
