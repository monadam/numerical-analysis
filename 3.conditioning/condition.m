fb = @(x) (1 + x.^-1).^-1;
Cb = @(x) (x + 1).^-2;
Kb = @(x) abs(x.^2 / (x+1).^3);

Plot(fb, Cb, Kb);

function Plot(f, C, K)
r = -10 : 1: 10;
m = @(r) 10.^(r / 10);
x = horzcat(1, m(r), -1 .* m(r));
% x gets more dense near 0

figure
hold on
plot(x, f(x));
plot(x, C(x));
plot(x, K(x));
end